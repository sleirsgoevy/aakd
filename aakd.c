#include <linux/module.h>
#include <linux/uaccess.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>

struct regs_stash
{
    uint64_t interrupt_number;
    uint64_t regs[16];
    uint64_t rip;
    uint64_t cs;
    uint64_t eflags;
    uint64_t ss;
};

static struct regs_stash* mailbox;
static struct wait_queue_head waitqueue;

#define MAILBOX_READ ((struct regs_stash*)1)
#define MAILBOX_WRITTEN ((struct regs_stash*)2)

static void inner_handler(struct regs_stash* stash)
{
    //mailbox goes from 0 to the current value of stash
    wait_event(waitqueue, __atomic_compare_exchange_n(&mailbox, &(struct regs_stash*[1]){0}, stash, 0, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST));
    //the debugger, when acknowledged, sets it to the value of MAILBOX_WRITTEN. wait for that and make it 0 again
    wait_event(waitqueue, __atomic_compare_exchange_n(&mailbox, &(struct regs_stash*[1]){MAILBOX_WRITTEN}, 0, 0, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST));
    //another thread could be blocked in the first line right now. wake it up
    wake_up(&waitqueue);
}

static uint64_t old_int1;
static uint64_t old_int3;
static uint64_t old_int14;

//these arguments should've been template parameters, but we're not in C++
//so let's just trust the optimizer and hope for the best...
static __always_inline void interrupt_handler(int isr_no, uint64_t* p_fallback_handler)
{
    if(isr_no == 14) //if constexpr
    {
        //page fault is special, as it is actually used by the kernel itself (and it pushes flags)
        //but we have to capture page faults if we want to support calling functions from gdb
        //the kernel should never have an execute fault though, so if we only capture execute faults we're safe (?)
        asm volatile(R"(
            testb $3, 16(%%rsp)
            jne 1f
            testb $16, (%%rsp)
            jne 2f
        1:
            jmp *%0
        2:
            add $8, %%rsp
        )"::"m"(*p_fallback_handler));
    }
    else
    {
        //check the mode at entry. if entered in ring3, call the original handler directly
        asm volatile(R"(
            testb $3, 8(%%rsp)
            je 1f
            jmp *%0
        1:
        )"::"m"(*p_fallback_handler));
    }
    //push the registers
    asm volatile(R"(
        pushq 32(%rsp)
        pushq 24(%rsp)
        pushq 24(%rsp)
        pushq 24(%rsp)
        push %r15
        push %r14
        push %r13
        push %r12
        push %r11
        push %r10
        push %r9
        push %r8
        push %rdi
        push %rsi
        push %rbp
        pushq 144(%rsp)
        push %rbx
        push %rdx
        push %rcx
        push %rax
    )");
    //push the interrupt number
    asm volatile("pushq %0"::"i"(isr_no));
    //move into the caller's stack. we don't want to reenable interrupts while being on an interrupt stack
    //even if we are there already, x86_64 interrupts leave no red zone, and gdb expects to have one, so we have to relocate anyway
    asm volatile(R"(
        mov 40(%rsp), %rax
        and $-16, %rax
        sub $436, %rax
        mov %rax, %rdi
        mov %rsp, %rsi
        mov $26, %rcx
        rep movsq
        mov %rax, %rsp
    )");
    //now that the stack is 16-byte aligned as expected, re-enable interrupts and call inner_handler
    asm volatile(R"(
        sti
        mov %%rsp, %%rdi
        call inner_handler
    )"::"m"(inner_handler));
    //pop the registers and return to the interrupted routing
    asm volatile(R"(
        pop %rax
        pop %rax
        pop %rcx
        pop %rdx
        pop %rbx
        pop 144(%rsp)
        pop %rbp
        pop %rsi
        pop %rdi
        pop %r8
        pop %r9
        pop %r10
        pop %r11
        pop %r12
        pop %r13
        pop %r14
        pop %r15
        pop 24(%rsp)
        pop 24(%rsp)
        pop 24(%rsp)
        pop 32(%rsp)
        iretq
    )");
}

__attribute__((naked))
static void int1_handler(void)
{
    interrupt_handler(1, &old_int1);
}

__attribute__((naked))
static void int3_handler(void)
{
    interrupt_handler(3, &old_int3);
}

__attribute__((naked))
static void int14_handler(void)
{
    interrupt_handler(14, &old_int14);
}

//yeah, copy_from_kernel_nofault is already a thing, i know...
static int copy_from_kernel(void* dst, const void* src, size_t sz)
{
    int err = 0;
    while(sz && !(err = __get_user(*(char*)dst, (const char __user*)src)))
    {
        dst++;
        src++;
        sz--;
    }
    return err;
}

//but here we probably really need a manual copy, 'coz we want to unset CR0.WP temporarily...
static int copy_to_kernel(void* dst, const void* src, size_t sz)
{
    uint64_t cr0, cr0_old;
    int err = 0;
    asm volatile("mov %%cr0, %0":"=r"(cr0));
    cr0_old = cr0;
    cr0 &= -65537; //unset CR0.WP
    asm volatile("mov %0, %%cr0"::"r"(cr0));
    while(sz && !(err = __put_user(*(const char*)src, (char __user*)dst)))
    {
        dst++;
        src++;
        sz--;
    }
    asm volatile("mov %%cr0, %0":"=r"(cr0));
    cr0 = (cr0 & -65537) | (cr0_old & 65536); //copy CR0.WP from old CR0
    asm volatile("mov %0, %%cr0"::"r"(cr0));
    return err;
}

static loff_t aakd_lseek(struct file* f, loff_t offset, int whence)
{
    if(offset < 0)
        return -EINVAL;
    else if(whence == SEEK_CUR)
        return f->f_pos += offset;
    else if(whence == SEEK_SET)
        return f->f_pos = offset;
    else
        return -EINVAL;
}

static ssize_t aakd_read(struct file* f, char __user* data, size_t size, loff_t* off)
{
    char buf[64];
    if(size > sizeof(buf))
        size = sizeof(buf);
    if(copy_from_kernel(buf, (void*)(0xff00000000000000ull|*off), size))
        return -EIO;
    if(copy_to_user(data, buf, size))
        return -EFAULT;
    *off += size;
    return size;
}

static ssize_t aakd_write(struct file* f, const char __user* data, size_t size, loff_t* off)
{
    char buf[64];
    if(size > sizeof(buf))
        size = sizeof(buf);
    if(copy_from_user(buf, data, size))
        return -EFAULT;
    if(copy_to_kernel((void*)(0xff00000000000000ull|*off), buf, size))
        return -EIO;
    *off += size;
    return size;
}

#define AAKD_READ_MAILBOX _IOR('A', 1, struct regs_stash)
#define AAKD_WRITE_MAILBOX _IOW('A', 1, struct regs_stash)

static long aakd_ioctl(struct file* f, unsigned int command, unsigned long arg)
{
    struct regs_stash* stash;
    int err;
    switch(command)
    {
    case AAKD_READ_MAILBOX:
        stash = __atomic_load_n(&mailbox, __ATOMIC_SEQ_CST);
        do
        {
            if(!stash || stash == MAILBOX_WRITTEN)
                return -ESRCH;
        }
        while(stash == MAILBOX_READ || !__atomic_compare_exchange_n(&mailbox, &stash, MAILBOX_READ, 0, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST));
        if(copy_to_user((struct regs_stash __user*)arg, stash, sizeof(*stash)))
            err = -EFAULT;
        else
            err = 0;
        __atomic_store_n(&mailbox, stash, __ATOMIC_SEQ_CST);
        return err;
    case AAKD_WRITE_MAILBOX:
        stash = __atomic_load_n(&mailbox, __ATOMIC_SEQ_CST);
        do
        {
            if(!stash || stash == MAILBOX_WRITTEN)
                return -ESRCH;
        }
        while(stash == MAILBOX_READ || !__atomic_compare_exchange_n(&mailbox, &stash, MAILBOX_READ, 0, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST));
        if(copy_from_user(stash, (struct regs_stash __user*)arg, sizeof(*stash)))
        {
            __atomic_store_n(&mailbox, stash, __ATOMIC_SEQ_CST);
            return -EFAULT;
        }
        else
        {
            //if written successfully, let the kernel thread continue
            __atomic_store_n(&mailbox, MAILBOX_WRITTEN, __ATOMIC_SEQ_CST);
            //wake up the thread, so that it continues
            wake_up(&waitqueue);
            return 0;
        }
    case 0x1234:
        for(;;);
    case 0x1235:
        asm volatile("int3");
        return 0;
    default:
        return -ENOTTY;
    }
}

static struct file_operations fops = {
    .llseek = aakd_lseek,
    .read = aakd_read,
    .write = aakd_write,
    .unlocked_ioctl = aakd_ioctl,
};

static struct miscdevice aakd_dev = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "aakd",
    .fops = &fops,
};

static __always_inline uint64_t get_idt(void)
{
    struct
    {
        uint16_t size;
        uint64_t ptr;
    } __attribute__((packed)) idtr;
    asm volatile("sidt %0":"=m"(idtr));
    return idtr.ptr;
}

static __always_inline uint64_t get_irq(int irq)
{
    uint64_t ans = 0;
    uint64_t idt = get_idt();
    memcpy(&ans, (void*)(idt + 16 * irq), 2);
    memcpy((void*)&ans + 2, (void*)(idt + 16 * irq + 6), 6);
    return ans;
}

static __always_inline void set_irq(int irq, uint64_t value)
{
    uint64_t buf[2];
    uint64_t buf_old[2];
    uint64_t buf_new[2];
    uint64_t cr0, cr0_old;
    uint64_t idt = get_idt();
    memcpy(buf, (void*)(idt + 16 * irq), 16);
    buf_old[0] = buf[0];
    buf_old[1] = buf[1];
    memcpy(buf, &value, 2);
    memcpy((void*)buf + 6, (void*)&value + 2, 6);
    asm volatile("mov %%cr0, %0":"=r"(cr0));
    cr0_old = cr0;
    cr0 &= -65537;
    asm volatile("mov %0, %%cr0"::"r"(cr0));
    asm volatile("cmpxchg16b (%%rdi)":"=a"(buf_new[0]),"=d"(buf_new[1]):"a"(buf_old[0]),"d"(buf_old[1]),"b"(buf[0]),"c"(buf[1]),"D"((void*)(idt+16*irq)):"memory");
    asm volatile("mov %%cr0, %0":"=r"(cr0));
    cr0 = (cr0 & -65537) | (cr0_old & 65536);
    asm volatile("mov %0, %%cr0"::"r"(cr0));
    BUG_ON(buf_new[0] != buf_old[0] || buf_new[1] != buf_old[1]);
}

static int aakd_init(void)
{
    int err;
    if((err = misc_register(&aakd_dev)))
        return err;
    init_waitqueue_head(&waitqueue);
    //backup the kernel's interrupt handlers, and overwrite with ours
    old_int1 = get_irq(1);
    old_int3 = get_irq(3);
    old_int14 = get_irq(14);
    set_irq(1, (uint64_t)int1_handler);
    set_irq(3, (uint64_t)int3_handler);
    set_irq(14, (uint64_t)int14_handler);
    return 0;
}

static void aakd_exit(void)
{
    //restore the old interrupt handlers
    //if anyone is still running code from our versions, well, we're fucked
    set_irq(1, (uint64_t)old_int1);
    set_irq(3, (uint64_t)old_int3);
    set_irq(14, (uint64_t)old_int14);
    misc_deregister(&aakd_dev);
}

module_init(aakd_init);
module_exit(aakd_exit);
MODULE_AUTHOR("Sergey Lisov <sleirsgoevy@gmail.com>");
MODULE_LICENSE("GPL");
