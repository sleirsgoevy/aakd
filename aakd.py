import fcntl, os, sys, errno, select

stdin = sys.stdin.buffer.raw
stdout = sys.stdout.buffer.raw

dummy_trap_frame = bytearray(168)
dummy_trap_frame[0] = 1 # int1
dummy_trap_frame[136:144] = 0xfa4eadd2fa4eadd2 .to_bytes(8, 'little') # 'FAKE ADDR FAKE ADDR' in hexspeak

def get_dummy_trap_frame():
    dummy_trap_frame[0] = 1
    return bytes(dummy_trap_frame)

def set_dummy_trap_frame(tf):
    dummy_trap_frame[:] = tf
    if dummy_trap_frame[153] & 1: # TF
        dummy_trap_frame[136:144] = (int.from_bytes(dummy_trap_frame[136:144], 'little') + 1).to_bytes(8, 'little')
    else:
        dummy_trap_frame[136:144] = (int.from_bytes(dummy_trap_frame[136:144], 'little') + 2).to_bytes(8, 'little')

def is_dummy_trap_frame(tf):
    # check if cs == 0. gdb cannot overwrite it
    return not any(tf[144:152])

if '--dummy' in sys.argv:
    get_trap_frame = get_dummy_trap_frame
    set_trap_frame = set_dummy_trap_frame
    text_symbol = 0xffffffff81000000
    def read_memory(addr, size):
        if addr % 2:
            pattern = b'\xcc\x90'
        else:
            pattern = b'\x90\xcc'
        return (pattern * ((size + 1) // 2))[:size]
    def write_memory(addr, data):
        return True
else:
    aakd = os.open('/dev/aakd', os.O_RDWR)
    text_symbol = int(next(i[0] for i in map(str.split, open('/proc/kallsyms')) if i[-1] == '_text'), 16)
    assert text_symbol
    def get_trap_frame():
        ans = bytearray(168)
        while True:
            try: fcntl.ioctl(aakd, 0x80a84101, ans)
            except OSError as e:
                if e.errno != errno.ESRCH: raise
            else: break
            if select.select([0], [], [], 0)[0]:
                # gdb requested an interrupt. we can't really interrupt the kernel, so just return a fake frame to make it (and the user) happy
                return b'\xff' + get_dummy_trap_frame()[1:]
        return bytes(ans)
    def set_trap_frame(x):
        if not is_dummy_trap_frame(x):
            fcntl.ioctl(aakd, 0x40a84101, x)
    def read_memory(addr, size):
        try: return os.pread(aakd, size, addr % 2**57)
        except OSError as e:
            if e.errno != errno.EIO: raise
            return b''
    def write_memory(addr, data):
        try: os.pwrite(aakd, data, addr % 2**57)
        except OSError as e:
            if e.errno != errno.EIO: raise
            return False
        return True

def getchar():
    c = stdin.read(1)
    if c == b'': raise EOFError
    return c

def recv_pkt():
    while getchar() != b'$': pass
    ans = b''
    while not ans.endswith(b'#'):
        ans += getchar()
    ans += getchar()
    ans += getchar()
    csum = int(ans[-2:], 16)
    ans = ans[:-3]
    assert sum(ans) % 256 == csum
    stdout.write(b'+')
    return ans

def send_pkt(x):
    pkt = memoryview(b'$' + x + b'#' + ('%02x' % (sum(x) % 256)).encode('ascii'))
    while pkt:
        pkt = pkt[stdout.write(pkt):]
    while getchar() != b'+': pass

def aakd_to_gdb(x):
    ans = b''
    ans += x[8:16] # rax
    ans += x[32:40] # rbx
    ans += x[16:32] # rcx, rdx
    ans += x[56:72] # rsi, rdi
    ans += x[48:56] # rbp
    ans += x[40:48] # rsp
    ans += x[72:144] # r8-r15, rip
    ans += x[152:156] # eflags
    ans += x[144:148] # cs
    ans += x[160:164] # ss
    return ans

def gdb_to_aakd(x, y):
    x[8:16] = y[:8] # rax
    x[16:32] = y[16:32] # rcx, rdx
    x[32:40] = y[8:16] # rbx
    x[40:48] = y[56:64] # rsp
    x[48:56] = y[48:56] # rbp
    x[56:72] = y[32:48] # rsi, rdi
    x[72:144] = y[64:136] # r8-r15, rip
    x[152:156] = y[136:140] # eflags
    # intentionally not copying cs/ss

def serve_file(blob, cmd):
    start, size = (int(i, 16) for i in cmd.rstrip(b'b').split(b','))
    start = min(len(blob), start)
    size = min(len(blob) - start, size)
    if size == 0:
        send_pkt(b'l')
    else:
        send_pkt(b'm' + blob[start:start+size])

def get_and_fix_trap_frame():
    trap_frame = bytearray(get_trap_frame())
    trap_frame[153] &= -2 # clear TF
    if trap_frame[0] == 3: # int3
        # this exception is trap-like, convert to fault-like for gdb
        trap_frame[136:144] = (int.from_bytes(trap_frame[136:144], 'little') - 1).to_bytes(8, 'little')
    return trap_frame

trap_frame = get_and_fix_trap_frame()

def send_trap_signal(trap_frame):
    if trap_frame[0] in (1, 3):
        send_pkt(b'T05')
    elif trap_frame[0] == 14:
        send_pkt(b'T0b')
    elif trap_frame[0] == 255: # SIGINT
        send_pkt(b'T02')
    else:
        assert False, "Unknown trap"

while True:
    pkt = recv_pkt()
    if pkt == b'?':
        send_trap_signal(trap_frame)
    elif pkt.startswith(b'qSupported:'):
        send_pkt(b'qXfer:features:read+')
    elif pkt == b'qAttached':
        send_pkt(b'1')
    elif pkt.startswith(b'qXfer:features:read:target.xml:'):
        serve_file(b'''<?xml version="1.0"?>
<!DOCTYPE target SYSTEM "gdb-target.dtd">
<target>
<architecture>i386:x86-64</architecture>
<osabi>GNU/Linux</osabi>
</target>
''', pkt.split(b':', 4)[4])
    elif pkt.startswith(b'qOffsets'):
        send_pkt(('TextSeg=%016x'%text_symbol).encode('ascii'))
    elif pkt == b'g':
        send_pkt(aakd_to_gdb(trap_frame).hex().encode('ascii'))
    elif pkt.startswith(b'G'):
        gdb_to_aakd(trap_frame, bytes.fromhex(pkt[1:].decode('ascii')))
        send_pkt(b'OK')
    elif pkt.startswith(b'm'):
        addr, size = (int(i, 16) for i in pkt[1:].split(b','))
        send_pkt(read_memory(addr, size).hex().encode('ascii'))
    elif pkt.startswith(b'M'):
        pkt, data = pkt.split(b':')
        addr, size = (int(i, 16) for i in pkt[1:].split(b','))
        data = bytes.fromhex(data.decode('ascii'))
        assert size == len(data)
        send_pkt(b'OK' if write_memory(addr, data) else b'E0e')
    elif pkt in (b's', b'c'):
        if pkt == b's':
            trap_frame[153] |= 1
        set_trap_frame(trap_frame)
        trap_frame = get_and_fix_trap_frame()
        send_trap_signal(trap_frame)
    elif pkt == b'D':
        set_trap_frame(trap_frame)
        send_pkt(b'OK')
        break
    else: # unsupported
        send_pkt(b'')
