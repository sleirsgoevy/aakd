# aakd

aakd ("another absurd kernel debugger") is a Linux kernel module for debugging the kernel locally on the same machine. It works by hooking the int1/int3 interrupt handlers and handling these exceptions when they occur in kernel mode (which does not normally happen).

## Building

Just type `make`. You should have the appropriate kernel headers installed for your kernel.

## Usage

If you have a debug build of your kernel (some distributions provide them, or build your own), do this:

```
insmod aakd.ko
gdb /path/to/vmlinux -ex 'target remote | python3 aakd.py'
```

If you don't have one, you can omit the `/path/to/vmlinux` argument, but you won't have any symbols in your debugging session, including function names.

Note that the very first stop is a fake one at address `0xfa4eadd2fa4eadd2`. This is done so that you can set whatever breakpoints you want before typing `cont`. If you press Ctrl+C within GDB you'll get the same fake stop, actually preempting the kernel is not implemented.

## Featuers

What works:

* breakpoints
* single-stepping
* reading/writing memory
* calling kernel functions within GDB expressions 

What does not work:

* watchpoints
* multi-thread debugging (the code is reasonably thread-safe, but GDB will think that all stops come from the same thread)
* handling in-kernel exceptions other than int1/int3
* probably anything else I've missed in this list...

## License

```
Copyright 2024 Sergey Lisov

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
```

(As this features a Linux kernel module, this has to be licensed under GPLv2.)
